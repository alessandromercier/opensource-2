| Package | How to install | 
| -| - |
| NumPy | yum install python3-numpy |
| SciPy | yum install python3-scipy | 
| SciKit-Learn | yum install python3-sckikit-learn | 
| pandas | pip3 install cython pandas |